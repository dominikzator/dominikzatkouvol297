﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SphereDrag : MonoBehaviour, IDragHandler
{
    public void OnDrag(PointerEventData eventData)
    {
        Vector3 newPos = MechanicsManager.Instance.mainCamera.ScreenToWorldPoint(Input.mousePosition);
        newPos = new Vector3(newPos.x, newPos.y, 0f);
        gameObject.transform.position = newPos;
    }
}
