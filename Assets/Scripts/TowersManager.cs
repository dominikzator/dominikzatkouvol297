﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TowersManager : MonoBehaviour
{
    public static TowersManager Instance;

    public GameObject towerPrefab;

    public TextMeshProUGUI towersCountText;

    public List<Tower> allTowers = new List<Tower>();

    public int towersCap;

    public bool allowSpawnNewTowers = true;

    public int TowersCount {
        get
        {
            return towersCount;
        }
        set
        {
            towersCount = value;
            UpdateTowersCount();
            if(towersCount >= towersCap)
            {
                allowSpawnNewTowers = false;
                ActivateAllTowers();
            }
        }
    }

    private int towersCount;

    private void ActivateAllTowers()
    {
        foreach (var tower in allTowers)
            tower.ActivateTower();
    }

    private void UpdateTowersCount()
    {
        towersCountText.text = "Towers: " + TowersCount.ToString();
    }
    private void Awake()
    {
        if (Instance == null)
            Instance = this;
    }
}
