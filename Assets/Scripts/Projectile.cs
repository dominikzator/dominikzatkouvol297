﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float minDistance;
    public float maxDistance;

    [HideInInspector]
    public Tower towerSpawned;

    private Vector3 startingPos;
    private float randomDistance;

    private void OnEnable()
    {
        startingPos = gameObject.transform.position;
        randomDistance = Random.Range(minDistance, maxDistance);
    }
    private void LateUpdate()
    {
        float distance = Vector3.Distance(startingPos, gameObject.transform.position);
        if (distance > randomDistance)
            OnDestination();
    }
    private void OnDestination()
    {
        TrySpawnTower();
        Destroy(gameObject);
    }
    private void TrySpawnTower()
    {
        if (!TowersManager.Instance.allowSpawnNewTowers)
            return;

        GameObject tower = Instantiate(TowersManager.Instance.towerPrefab, gameObject.transform.position, Quaternion.identity) as GameObject;
    }
}
