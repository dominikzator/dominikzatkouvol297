﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public GameObject projectilePrefab;

    public Color activeColor;
    public Color inactiveColor;

    public float minRandomAngle;
    public float maxRandomAngle;

    public float shootingPower;

    public float repeatRateForRotation;

    public float spawnedTowerActivationTimeInSeconds;

    public int maxShootCount;

    public bool isFirstTower;

    private SpriteRenderer spriteRenderer;

    private int shootingCounter = 0;

    public void ActivateTower()
    {
        CancelInvoke(nameof(CyclicBehaviour));
        shootingCounter = 0;
        spriteRenderer.color = activeColor;
        InvokeRepeating(nameof(CyclicBehaviour), 0f, repeatRateForRotation);
    }

    private void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    private void Start()
    {
        if (isFirstTower)
            ActivateTower();
        else
            Invoke(nameof(ActivateTower), spawnedTowerActivationTimeInSeconds);
    }
    private void DeactivateTower()
    {
        spriteRenderer.color = inactiveColor;
        CancelInvoke(nameof(CyclicBehaviour));
    }
    private void CyclicBehaviour()
    {
        RotateRandomly();
        Shoot();
    }
    private void RotateRandomly()
    {
        float randomAngle = Random.Range(minRandomAngle, maxRandomAngle);
        transform.Rotate(new Vector3(0f, 0f, randomAngle));
    }
    private void Shoot()
    {
        GameObject ob = Instantiate(projectilePrefab, gameObject.transform.position, Quaternion.identity) as GameObject;
        Projectile projectile = ob.GetComponent<Projectile>();
        projectile.towerSpawned = this;
        Rigidbody2D rigidbody2D = ob.GetComponent<Rigidbody2D>();
        Vector3 direction = gameObject.transform.right;
        rigidbody2D.AddForce(direction * shootingPower);

        ++shootingCounter;
        if (shootingCounter >= maxShootCount)
            DeactivateTower();
    }
    private void OnEnable()
    {
        ++TowersManager.Instance.TowersCount;
        TowersManager.Instance.allTowers.Add(this);
    }
    private void OnDisable()
    {
        --TowersManager.Instance.TowersCount;
        TowersManager.Instance.allTowers.Remove(this);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.GetComponent<Projectile>().towerSpawned != this)
        {
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
    }
}
