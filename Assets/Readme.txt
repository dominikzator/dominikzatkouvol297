Zadanie 2 - Bonus

Ze względu na wielokrotne używanie złożonej obliczeniowo funkcji Instantiate dla instancjonowania Wież i Pocisków, zastosowanie ObjectPooling dla Wież oraz Pocisków jest obowiązkowe w celach optymalizacyjnych. Niestety z powodu braku czasu nie zdążyłem już zaimplementować ObjectPoolingu dla podanego zadania.